// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCmCAtRTe_7xmNJRdDtk_67WFWiGg-AJf8",
    authDomain: "taekwondo-f7a77.firebaseapp.com",
    databaseURL: "https://taekwondo-f7a77.firebaseio.com",
    projectId: "taekwondo-f7a77",
    storageBucket: "taekwondo-f7a77.appspot.com",
    messagingSenderId: "500446817901"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
