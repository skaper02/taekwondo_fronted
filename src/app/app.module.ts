import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";

import { AppRoutingModule, routingModules } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LoginComponent } from "./components/login/login.component";
import { Material } from "./material";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GraphQLModule } from "./graphql.module";
import { HttpClientModule } from "@angular/common/http";
import { LayoutModule } from "@angular/cdk/layout";
import { InstructoresListComponent } from "./components/instructores/instructores-list/instructores-list.component";
import { InstructoresComponent } from "./components/instructores/instructores/instructores.component";
import { InstructoresViewComponent } from "./components/instructores/instructores-view/instructores-view.component";
import { environment } from "../environments/environment";
import { AngularFirestore } from "angularfire2/firestore";
import { LoadingSpinerComponent } from "./components/ui/loading-spiner/loading-spiner.component";
import { AlumnoViewComponent } from "./components/alumnos/alumno-view/alumno-view.component";
import { AlumnosComponent } from "./components/alumnos/alumno/alumnos.component";
import { AlumnoAddEditComponent } from "./components/alumnos/alumno-add-edit/alumno-add-edit.component";
import { AddHorarioComponent } from "./components/horarios/add-horario/add-horario.component";
import { HorariosComponent } from "./components/horarios/horarios.component";
import { ViewHorarioComponent } from "./components/horarios/view-horario/view-horario.component";
import { AddFormComponent } from "./components/torneos/add-form/add-form.component";
import { TorneosComponent } from "./components/torneos/torneos.component";
import { ViewTorneoComponent } from "./components/torneos/view-torneo/view-torneo.component";
import { AddAlumnoComponent } from "./components/torneos/add-alumno/add-alumno.component";
import { AddPagoComponent } from "./components/pagos/add-pago/add-pago.component";
@NgModule({
  declarations: [
    AppComponent,
    routingModules,
    InstructoresListComponent,
    InstructoresComponent,
    InstructoresViewComponent,
    LoadingSpinerComponent,
    AlumnoViewComponent,
    AlumnoAddEditComponent,
    AddHorarioComponent,
    ViewHorarioComponent,
    AddFormComponent,
    ViewTorneoComponent,
    AddAlumnoComponent,
    AddPagoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    Material,
    GraphQLModule,
    HttpClientModule,
    ReactiveFormsModule,
    LayoutModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent],
  entryComponents: [
    InstructoresComponent,
    InstructoresViewComponent,
    AlumnoAddEditComponent,
    AlumnoViewComponent,
    AddHorarioComponent,
    ViewHorarioComponent,
    AddFormComponent,
    ViewTorneoComponent,
    AddAlumnoComponent,
    AddPagoComponent
  ]
})
export class AppModule {}
