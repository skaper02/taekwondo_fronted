import { Injectable } from "@angular/core";
import { AngularFireList } from "angularfire2/database";
import { AngularFirestore } from "angularfire2/firestore";
import gql from "graphql-tag";
import { Apollo } from "apollo-angular";
import { AuthService } from "./auth.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";

//
// ─── QUERIES ────────────────────────────────────────────────────────────────────
//

const alumnoQuery = gql`
  query($id: String!) {
    getAlumno(id: $id) {
      id
      nombre
      primerApellido
      segundoApellido
      fecha_nacimiento
      sexo
      n_telefono
      correo
      grado
      nombre_Calle
      nCasa
      codigoPostal
      ciudad
      municipio
      asentamiento
      tipo_asentamiento
      fakeId
      clase
    }
  }
`;

const alumnosPerGrado = gql`
  query($grado: String!) {
    getAlumnosPerGrado(grado: $grado) {
      nombre
      primerApellido
      segundoApellido
      grado
      fakeId
      n_telefono
    }
  }
`;

const fullNameQuery = gql`
  query($id: String!) {
    getAlumno(id: $id) {
      nombre
      primerApellido
      segundoApellido
    }
  }
`;

//
// ─── MUTATIONS ──────────────────────────────────────────────────────────────────
//

const changeStatus = gql`
  mutation($id: String!, $status: Boolean!) {
    changeStatus(id: $id, status: $status) {
      status
      path
      message
    }
  }
`;

const deleteMutation = gql`
  mutation($id: String!) {
    deleteAlumno(id: $id) {
      status
      path
      message
    }
  }
`;

const registerMutation = gql`
  mutation registerAlumno(
    $nombre: String!
    $primerApellido: String!
    $segundoApellido: String!
    $fecha_nacimiento: String!
    $sexo: String!
    $n_telefono: String!
    $correo: String!
    $grado: String!
    $nombre_Calle: String!
    $nCasa: String!
    $codigoPostal: String!
    $ciudad: String!
    $municipio: String!
    $asentamiento: String!
    $tipo_asentamiento: String!
    $clase: String!
  ) {
    registerAlumno(
      alumno: {
        nombre: $nombre
        primerApellido: $primerApellido
        segundoApellido: $segundoApellido
        fecha_nacimiento: $fecha_nacimiento
        sexo: $sexo
        n_telefono: $n_telefono
        correo: $correo
        grado: $grado
        nombre_Calle: $nombre_Calle
        nCasa: $nCasa
        codigoPostal: $codigoPostal
        ciudad: $ciudad
        municipio: $municipio
        asentamiento: $asentamiento
        tipo_asentamiento: $tipo_asentamiento
        clase: $clase
      }
    ) {
      status
      path
      message
    }
  }
`;

const editMutation = gql`
  mutation editAlumno(
    $id: String!
    $nombre: String!
    $primerApellido: String!
    $segundoApellido: String!
    $fecha_nacimiento: String!
    $sexo: String!
    $n_telefono: String!
    $correo: String!
    $grado: String!
    $nombre_Calle: String!
    $nCasa: String!
    $codigoPostal: String!
    $ciudad: String!
    $municipio: String!
    $asentamiento: String!
    $tipo_asentamiento: String!
    $clase: String!
  ) {
    editAlumno(
      alumno: {
        id: $id
        nombre: $nombre
        primerApellido: $primerApellido
        segundoApellido: $segundoApellido
        fecha_nacimiento: $fecha_nacimiento
        sexo: $sexo
        n_telefono: $n_telefono
        correo: $correo
        grado: $grado
        nombre_Calle: $nombre_Calle
        nCasa: $nCasa
        codigoPostal: $codigoPostal
        ciudad: $ciudad
        municipio: $municipio
        asentamiento: $asentamiento
        tipo_asentamiento: $tipo_asentamiento
        clase: $clase
      }
    ) {
      status
      path
      message
    }
  }
`;
@Injectable({
  providedIn: "root"
})
export class AlumnoService {
  public form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    nombre: new FormControl("", Validators.required),
    primerApellido: new FormControl("", Validators.required),
    segundoApellido: new FormControl(null),
    n_telefono: new FormControl("", [
      Validators.required,
      Validators.compose([Validators.minLength(10), Validators.maxLength(10)])
    ]),
    fecha_nacimiento: new FormControl("", Validators.required),
    correo: new FormControl("", [Validators.required, Validators.email]),
    grado: new FormControl("", [Validators.required]),
    genero: new FormControl(1, Validators.required),
    nombre_Calle: new FormControl("", Validators.required),
    nCasa: new FormControl("", Validators.required),
    codigoPostal: new FormControl("", [
      Validators.required,
      Validators.pattern("^[0-9]*$")
    ]),
    ciudad: new FormControl("", Validators.required),
    municipio: new FormControl("", Validators.required),
    asentamiento: new FormControl("", Validators.required),
    tipo_asentamiento: new FormControl("", Validators.required),
    clase: new FormControl("", Validators.required)
  });

  alumnoList: AngularFireList<any>;
  constructor(
    private firestore: AngularFirestore,
    private _apollo: Apollo,
    private _authService: AuthService
  ) {}

  getAlumno(id: string) {
    return this._apollo.query({
      query: alumnoQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  getAlumnoPerGrado(grado: string) {
    return this._apollo.query({
      query: alumnosPerGrado,
      variables: {
        grado: grado
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  getFullNameAlumno(id: string) {
    return this._apollo.query({
      query: fullNameQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  setFormData(data: any) {
    this.form.setValue({
      $key: data.id,
      nombre: data.nombre,
      primerApellido: data.primerApellido,
      segundoApellido: data.segundoApellido,
      n_telefono: data.n_telefono,
      fecha_nacimiento: data.fecha_nacimiento,
      correo: data.correo,
      grado: data.grado,
      genero: data.sexo,
      nombre_Calle: data.nombre_Calle,
      nCasa: data.nCasa,
      codigoPostal: data.codigoPostal,
      ciudad: data.ciudad,
      municipio: data.municipio,
      asentamiento: data.asentamiento,
      tipo_asentamiento: data.tipo_asentamiento,
      clase: data.clase
    });
  }

  getAlumnos() {
    return this.firestore.collection("alumnos").snapshotChanges();
  }

  registerAlumno(data: any) {
    data.n_telefono = "+52" + data.n_telefono;
    data.sexo = data.genero;
    delete data.genero;
    return this._apollo.mutate({
      mutation: registerMutation,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  changeStatus(id: string, status: boolean) {
    return this._apollo.mutate({
      mutation: changeStatus,
      variables: {
        id: id,
        status: status
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  editAlumno(data: any, id: string) {
    data.n_telefono = "+52" + data.n_telefono;
    data.id = id;
    data.sexo = data.genero;
    return this._apollo.mutate({
      mutation: editMutation,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  deleteAlumno(id: string) {
    return this._apollo.mutate({
      mutation: deleteMutation,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }
}
