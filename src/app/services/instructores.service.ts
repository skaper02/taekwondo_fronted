import { Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import gql from "graphql-tag";
import { AuthService } from "./auth.service";
import { Apollo } from "apollo-angular";
import { AngularFirestore } from "angularfire2/firestore";
import { AngularFireList } from "angularfire2/database";
import { validateConfig } from "@angular/router/src/config";

const instructorTableDataQuery = gql`
  query {
    allInstructors {
      _id
      nombre
      primerApellido
      segundoApellido
      correo
    }
  }
`;

const meQuery = gql`
  query {
    me {
      _id
      tipo
      nombre
      primerApellido
      segundoApellido
      fecha_nacimiento
      sexo
      n_telefono
      correo
      grado
      password
      nombre_Calle
      nCasa
      codigoPostal
      tipo
      estado
      ciudad
      municipio
      asentamiento
      tipo_asentamiento
    }
  }
`;

const specificInstructorQuery = gql`
  query instructor($id: String!) {
    instructor(id: $id) {
      _id
      nombre
      primerApellido
      segundoApellido
      fecha_nacimiento
      sexo
      n_telefono
      correo
      grado
      password
      nombre_Calle
      nCasa
      codigoPostal
      estado
      ciudad
      municipio
      asentamiento
      tipo_asentamiento
    }
  }
`;

const specificInstructorResumeQuery = gql`
  query instructor($id: String!) {
    instructor(id: $id) {
      nombre
      primerApellido
      segundoApellido
    }
  }
`;

const deleteInstructorMutation = gql`
  mutation delete($id: String!) {
    deleteInstructor(id: $id) {
      status
      path
      message
    }
  }
`;

const registerInstructorMutation = gql`
  mutation registerInstructor(
    $nombre: String!
    $primerApellido: String!
    $segundoApellido: String
    $fecha_nacimiento: String!
    $sexo: String!
    $n_telefono: String!
    $correo: String!
    $grado: String!
    $password: String!
    $nombre_Calle: String!
    $nCasa: String!
    $codigoPostal: String!
    $ciudad: String!
    $municipio: String!
    $asentamiento: String!
    $tipo_asentamiento: String!
  ) {
    registerInstructor(
      instructor: {
        nombre: $nombre
        primerApellido: $primerApellido
        segundoApellido: $segundoApellido
        fecha_nacimiento: $fecha_nacimiento
        sexo: $sexo
        n_telefono: $n_telefono
        correo: $correo
        grado: $grado
        password: $password
        nombre_Calle: $nombre_Calle
        nCasa: $nCasa
        codigoPostal: $codigoPostal
        ciudad: $ciudad
        municipio: $municipio
        asentamiento: $asentamiento
        tipo_asentamiento: $tipo_asentamiento
      }
    ) {
      status
      message
    }
  }
`;

const editInstructorMutation = gql`
  mutation editInstructor(
    $id: String!
    $nombre: String!
    $primerApellido: String!
    $segundoApellido: String
    $fecha_nacimiento: String!
    $sexo: String!
    $n_telefono: String!
    $correo: String!
    $grado: String!
    $nombre_Calle: String!
    $nCasa: String!
    $codigoPostal: String!
    $ciudad: String!
    $municipio: String!
    $asentamiento: String!
    $tipo_asentamiento: String!
  ) {
    editInstructor(
      instructor: {
        id: $id
        nombre: $nombre
        primerApellido: $primerApellido
        segundoApellido: $segundoApellido
        fecha_nacimiento: $fecha_nacimiento
        sexo: $sexo
        n_telefono: $n_telefono
        correo: $correo
        grado: $grado
        nombre_Calle: $nombre_Calle
        nCasa: $nCasa
        codigoPostal: $codigoPostal
        ciudad: $ciudad
        municipio: $municipio
        asentamiento: $asentamiento
        tipo_asentamiento: $tipo_asentamiento
      }
    ) {
      status
      message
    }
  }
`;

const updatePassMutation = gql`
  mutation($id: String!, $password: String!) {
    updatePassword(id: $id, password: $password)
  }
`;

@Injectable({
  providedIn: "root"
})
export class InstructoresService {
  public form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    nombre: new FormControl("", Validators.required),
    primerApellido: new FormControl("", Validators.required),
    segundoApellido: new FormControl(null),
    n_telefono: new FormControl("", [
      Validators.required,
      Validators.compose([Validators.minLength(10), Validators.maxLength(10)])
    ]),
    fecha_nacimiento: new FormControl("", Validators.required),
    correo: new FormControl("", [Validators.required, Validators.email]),
    grado: new FormControl("", [Validators.required]),
    genero: new FormControl(1, Validators.required),
    nombre_Calle: new FormControl("", Validators.required),
    nCasa: new FormControl("", Validators.required),
    codigoPostal: new FormControl("", [
      Validators.required,
      Validators.pattern("^[0-9]*$")
    ]),
    ciudad: new FormControl("", Validators.required),
    municipio: new FormControl("", Validators.required),
    asentamiento: new FormControl("", Validators.required),
    tipo_asentamiento: new FormControl("", Validators.required)
  });

  // Firebase list
  instructorList: AngularFireList<any>;

  constructor(
    private _authService: AuthService,
    private _apollo: Apollo,
    private firestore: AngularFirestore
  ) {}

  getInstructors() {
    return this.firestore.collection("users").snapshotChanges();
  }

  instructorDataTable() {
    return this._apollo.query({
      query: instructorTableDataQuery,
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  setMeData(data: any) {
    this.form.setValue({
      $key: "",
      nombre: data.me.nombre,
      primerApellido: data.me.primerApellido,
      segundoApellido: data.me.segundoApellido,
      n_telefono: data.me.n_telefono,
      fecha_nacimiento: data.me.fecha_nacimiento,
      correo: data.me.correo,
      grado: data.me.grado,
      genero: data.me.sexo,
      nombre_Calle: data.me.nombre_Calle,
      nCasa: data.me.nCasa,
      codigoPostal: data.me.codigoPostal,
      ciudad: data.me.ciudad,
      municipio: data.me.municipio,
      asentamiento: data.me.asentamiento,
      tipo_asentamiento: data.me.tipo_asentamiento
    });
  }

  setData(data: any) {
    this.form.setValue({
      $key: "",
      nombre: data.instructor.nombre,
      primerApellido: data.instructor.primerApellido,
      segundoApellido: data.instructor.segundoApellido,
      n_telefono: data.instructor.n_telefono,
      fecha_nacimiento: data.instructor.fecha_nacimiento,
      correo: data.instructor.correo,
      grado: data.instructor.grado,
      genero: data.instructor.sexo,
      nombre_Calle: data.instructor.nombre_Calle,
      nCasa: data.instructor.nCasa,
      codigoPostal: data.instructor.codigoPostal,
      ciudad: data.instructor.ciudad,
      municipio: data.instructor.municipio,
      asentamiento: data.instructor.asentamiento,
      tipo_asentamiento: data.instructor.tipo_asentamiento
    });
  }

  me() {
    return this._apollo.query({
      query: meQuery,
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  specificInstructor(id: string) {
    return this._apollo.query({
      query: specificInstructorQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  specificInstructorResume(id: string) {
    return this._apollo.query({
      query: specificInstructorResumeQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  deleteInstructor(id: string) {
    return this._apollo.mutate({
      mutation: deleteInstructorMutation,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  editInstructor(form: FormGroup, id: string) {
    return this._apollo.mutate({
      mutation: editInstructorMutation,
      variables: {
        id: id,
        nombre: form.value.nombre,
        primerApellido: form.value.primerApellido,
        segundoApellido: form.value.segundoApellido,
        fecha_nacimiento: form.value.fecha_nacimiento,
        sexo: form.value.genero + "",
        n_telefono: "+52" + form.value.n_telefono,
        correo: form.value.correo,
        grado: form.value.grado,
        nombre_Calle: form.value.nombre_Calle,
        nCasa: form.value.nCasa,
        codigoPostal: form.value.codigoPostal,
        ciudad: form.value.ciudad,
        municipio: form.value.municipio,
        asentamiento: form.value.asentamiento,
        tipo_asentamiento: form.value.tipo_asentamiento
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  registerInstructor(form: FormGroup) {
    return this._apollo.mutate({
      mutation: registerInstructorMutation,
      variables: {
        nombre: form.value.nombre,
        primerApellido: form.value.primerApellido,
        segundoApellido: form.value.segundoApellido,
        fecha_nacimiento: form.value.fecha_nacimiento,
        sexo: form.value.genero + "",
        n_telefono: "+52" + form.value.n_telefono,
        correo: form.value.correo,
        grado: form.value.grado,
        password: "null1211312",
        nombre_Calle: form.value.nombre_Calle,
        nCasa: form.value.nCasa,
        codigoPostal: form.value.codigoPostal,
        ciudad: form.value.ciudad,
        municipio: form.value.municipio,
        asentamiento: form.value.asentamiento,
        tipo_asentamiento: form.value.tipo_asentamiento
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  updatePassword(id: string, password: string) {
    return this._apollo.mutate({
      mutation: updatePassMutation,
      variables: {
        id: id,
        password: password
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }
}
