import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { Apollo } from "apollo-angular";
import * as firebase from "firebase";
import gql from "graphql-tag";

const query = gql`
  query($email: String!) {
    emailVerified(email: $email)
  }
`;
const mutation = gql`
  mutation($email: String!) {
    forgetPassword(email: $email)
  }
`;
@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private _router: Router, private _apollo: Apollo) {}

  login(form: FormGroup) {
    return firebase
      .auth()
      .signInWithEmailAndPassword(
        form.value.emailFormControl,
        form.value.passwordFormControl
      );
  }

  forgetPassword(email: string) {
    return this._apollo.mutate({
      mutation: mutation,
      variables: {
        email: email
      }
    });
  }

  isVerifiedEmail(email: string) {
    return this._apollo.query({
      query: query,
      variables: {
        email: email
      }
    });
  }

  getCurrentId() {
    return firebase.auth().currentUser.getIdToken(true);
  }

  getCurrentUID() {
    return firebase.auth().currentUser.uid;
  }

  async logoutToken() {
    return firebase.auth().signOut();
  }

  logoutUser() {
    localStorage.removeItem("token");
    this._router.navigate(["/login"]);
  }

  getToken() {
    return localStorage.getItem("token");
  }

  loggedIn() {
    return !!localStorage.getItem("token");
  }
}
