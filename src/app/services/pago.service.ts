import { Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AngularFireList } from "angularfire2/database";
import { AngularFirestore } from "angularfire2/firestore";
import { Apollo } from "apollo-angular";
import { AuthService } from "./auth.service";
import gql from "graphql-tag";

const getPago = gql`
  query($id: String!) {
    getPago(id: $id) {
      id
      id_alumno
      monto
      fecha
      concepto
    }
  }
`;

const registerPago = gql`
  mutation registerPago(
    $id_alumno: String!
    $monto: Float!
    $fecha: String!
    $concepto: String!
  ) {
    registerPago(
      pago: {
        id_alumno: $id_alumno
        monto: $monto
        fecha: $fecha
        concepto: $concepto
      }
    ) {
      path
      message
      status
    }
  }
`;

const updatePago = gql`
  mutation updatePago(
    $id: String!
    $id_alumno: String!
    $monto: Float!
    $fecha: String!
    $concepto: String!
  ) {
    updatePago(
      pago: {
        id: $id
        id_alumno: $id_alumno
        monto: $monto
        fecha: $fecha
        concepto: $concepto
      }
    ) {
      path
      message
      status
    }
  }
`;

@Injectable({
  providedIn: "root"
})
export class PagoService {
  public form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    id_alumno: new FormControl("", Validators.required),
    monto: new FormControl("", Validators.required),
    fecha: new FormControl("", Validators.required),
    concepto: new FormControl("", Validators.required)
  });

  pagosList: AngularFireList<any>;

  constructor(
    private firestore: AngularFirestore,
    private _apollo: Apollo,
    private _authService: AuthService
  ) {}

  setFormData(data: any) {
    this.form.setValue({
      $key: data.id,
      id_alumno: data.id_alumno,
      monto: data.monto,
      fecha: data.fecha,
      concepto: data.concepto
    });
  }

  getPagos() {
    return this.firestore.collection("pagos").snapshotChanges();
  }

  getPago(id: string) {
    return this._apollo.query({
      query: getPago,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  registerPago(data: any) {
    return this._apollo.mutate({
      mutation: registerPago,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  updatePago(data: any) {
    return this._apollo.mutate({
      mutation: updatePago,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }
}
