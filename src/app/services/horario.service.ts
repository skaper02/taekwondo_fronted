import { Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AngularFireList } from "angularfire2/database";
import { AngularFirestore } from "angularfire2/firestore";
import { Apollo } from "apollo-angular";
import { AuthService } from "./auth.service";
import gql from "graphql-tag";

//
// ─── MUTATIONS ──────────────────────────────────────────────────────────────────
//

const addClaseMutation = gql`
  mutation($hora_inicio: String!, $hora_fin: String!, $instructor: String!) {
    addClase(
      clase: {
        hora_inicio: $hora_inicio
        hora_termino: $hora_fin
        id_instructor: $instructor
      }
    ) {
      status
      path
      message
    }
  }
`;

const editClaseMutation = gql`
  mutation(
    $id: String!
    $hora_inicio: String!
    $hora_fin: String!
    $instructor: String!
  ) {
    editClase(
      clase: {
        id: $id
        hora_inicio: $hora_inicio
        hora_termino: $hora_fin
        id_instructor: $instructor
      }
    ) {
      status
      path
      message
    }
  }
`;

const deleteClaseMutation = gql`
  mutation($id: String!) {
    deleteClase(id_clase: $id) {
      status
      path
      message
    }
  }
`;

//
// ─── QUERIES ────────────────────────────────────────────────────────────────────
//

const getClaseQuery = gql`
  query getClase($id: String!) {
    getClase(id: $id) {
      hora_inicio
      hora_termino
      instructor
    }
  }
`;

const getClasesQuery = gql`
  query {
    allClases {
      id
      instructor
      hora_inicio
      hora_termino
    }
  }
`;

const getAlumnosPerClaseQuery = gql`
  query getAlumnosPerClase($id: String!) {
    getAlumnosPerClase(id_clase: $id) {
      nombre
      primerApellido
      segundoApellido
      grado
      fakeId
    }
  }
`;

@Injectable({
  providedIn: "root"
})
export class HorarioService {
  public form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    hora_inicio: new FormControl("", Validators.required),
    hora_fin: new FormControl("", Validators.required),
    instructor: new FormControl("", Validators.required)
  });

  clase: AngularFireList<any>;
  constructor(
    private firestore: AngularFirestore,
    private _apollo: Apollo,
    private _authService: AuthService
  ) {}

  setFormData(data: any) {
    this.form.setValue({
      $key: "a",
      hora_inicio: data.hora_inicio,
      hora_fin: data.hora_termino,
      instructor: data.instructor
    });
  }

  getClases() {
    return this.firestore.collection("clase").snapshotChanges();
  }

  getClase(id: string) {
    return this._apollo.query({
      query: getClaseQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  getAlumnosPerClase(id: string) {
    return this._apollo.query({
      query: getAlumnosPerClaseQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  getAllClases() {
    return this._apollo.query({
      query: getClasesQuery,
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  addClase(data: any) {
    return this._apollo.mutate({
      mutation: addClaseMutation,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  editClase(data: any) {
    return this._apollo.mutate({
      mutation: editClaseMutation,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }

  deleteClase(id_clase: string) {
    return this._apollo.mutate({
      mutation: deleteClaseMutation,
      variables: {
        id: id_clase
      },
      context: {
        headers: {
          authorization: this._authService.getToken()
        }
      }
    });
  }
}
