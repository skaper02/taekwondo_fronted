import { Injectable } from "@angular/core";
import { AngularFireList } from "angularfire2/database";
import { AngularFirestore } from "angularfire2/firestore";
import { Apollo } from "apollo-angular";
import { AuthService } from "./auth.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import gql from "graphql-tag";

//
// ─── QUERIES ────────────────────────────────────────────────────────────────────
//

const getTorneoQuery = gql`
  query getTorneo($id: String!) {
    getTorneo(id: $id) {
      id
      nombre
      fecha
      lugar
      hora
    }
  }
`;

const getAlumnosPerTorneo = gql`
  query getAlumnosPerTorneo($id_torneo: String!) {
    getAlumnosPerTorneo(id_torneo: $id_torneo) {
      id
      fakeId
      nombre
      primerApellido
      segundoApellido
      n_telefono
      grado
    }
  }
`;

//
// ─── MUTATIONS ──────────────────────────────────────────────────────────────────
//

const deleteTorneo = gql`
  mutation($id: String!) {
    deleteTorneo(id: $id) {
      status
      path
      message
    }
  }
`;

const registerTorneo = gql`
  mutation($nombre: String!, $lugar: String!, $fecha: String!, $hora: String!) {
    registerTorneo(
      torneo: { nombre: $nombre, lugar: $lugar, fecha: $fecha, hora: $hora }
    ) {
      status
      path
      message
    }
  }
`;

const editTorneo = gql`
  mutation(
    $id: String!
    $nombre: String!
    $lugar: String!
    $fecha: String!
    $hora: String!
  ) {
    editTorneo(
      torneo: {
        id: $id
        nombre: $nombre
        lugar: $lugar
        fecha: $fecha
        hora: $hora
      }
    ) {
      status
      path
      message
    }
  }
`;

const addAlumnoTorneo = gql`
  mutation($id_torneo: String!, $id_alumno: String!) {
    addAlumnoTorneo(id_torneo: $id_torneo, id_alumno: $id_alumno) {
      path
      message
      status
    }
  }
`;

const deleteAlumnoTorneo = gql`
  mutation($id_torneo: String!, $id_alumno: String!) {
    deleteAlumnoTorneo(id_torneo: $id_torneo, id_alumno: $id_alumno) {
      path
      message
      status
    }
  }
`;
@Injectable({
  providedIn: "root"
})
export class TorneoService {
  public form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    nombre: new FormControl("", Validators.required),
    fecha: new FormControl("", Validators.required),
    hora: new FormControl("", Validators.required),
    lugar: new FormControl("", Validators.required)
  });

  torneoList: AngularFireList<any>;
  constructor(
    private firestore: AngularFirestore,
    private _apollo: Apollo,
    private _auth: AuthService
  ) {}

  public getTorneos() {
    return this.firestore.collection("torneos").snapshotChanges();
  }

  setFormData(data: any) {
    this.form.setValue({
      $key: data.id || "",
      nombre: data.nombre,
      fecha: data.fecha,
      hora: data.hora,
      lugar: data.lugar
    });
  }

  getTorneo(id: string) {
    return this._apollo.query({
      query: getTorneoQuery,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      }
    });
  }

  getAlumnosPerTorneo(id: string) {
    return this._apollo.query({
      query: getAlumnosPerTorneo,
      variables: {
        id_torneo: id
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      },
      fetchPolicy: "network-only"
    });
  }

  deleteTorneo(id: string) {
    return this._apollo.mutate({
      mutation: deleteTorneo,
      variables: {
        id: id
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      }
    });
  }

  registerTorneo(data: any) {
    return this._apollo.mutate({
      mutation: registerTorneo,
      variables: {
        ...data
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      }
    });
  }

  editTorneo(data: any, id: string) {
    return this._apollo.mutate({
      mutation: editTorneo,
      variables: {
        ...data,
        id: id
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      }
    });
  }

  addAlumnoTorneo(id_torneo: string, id_alumno: string) {
    return this._apollo.mutate({
      mutation: addAlumnoTorneo,
      variables: {
        id_torneo: id_torneo,
        id_alumno: id_alumno
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      }
    });
  }

  deleteAlumnoTorneo(id_torneo: string, id_alumno: string) {
    return this._apollo.mutate({
      mutation: deleteAlumnoTorneo,
      variables: {
        id_torneo: id_torneo,
        id_alumno: id_alumno
      },
      context: {
        headers: {
          authorization: this._auth.getToken()
        }
      }
    });
  }
}
