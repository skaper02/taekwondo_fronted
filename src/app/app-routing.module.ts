import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { AuthGuard } from "./services/auth.guard";
import { MainNavComponent } from "./components/main-nav/main-nav.component";
import { InstructoresListComponent } from "./components/instructores/instructores-list/instructores-list.component";
import { AlumnosComponent } from "./components/alumnos/alumno/alumnos.component";
import { InscripcionesComponent } from "./components/inscripciones/inscripciones.component";
import { HorariosComponent } from "./components/horarios/horarios.component";
import { TorneosComponent } from "./components/torneos/torneos.component";
import { PagosComponent } from "./components/pagos/pagos.component";

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  {
    path: "dashboard",
    component: MainNavComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "instructores",
        component: InstructoresListComponent
      },
      {
        path: "alumnos",
        component: AlumnosComponent
      },
      {
        path: "inscripciones",
        component: InscripcionesComponent
      },
      {
        path: "horarios",
        component: HorariosComponent
      },
      {
        path: "torneos",
        component: TorneosComponent
      },
      {
        path: "pagos",
        component: PagosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingModules = [
  LoginComponent,
  InstructoresListComponent,
  MainNavComponent,
  AlumnosComponent,
  InscripcionesComponent,
  HorariosComponent,
  TorneosComponent,
  PagosComponent
];
