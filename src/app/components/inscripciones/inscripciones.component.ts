import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatDialogConfig,
  MatPaginator,
  MatDialog,
  MatDialogRef
} from "@angular/material";
import { AlumnoService } from "src/app/services/alumno.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { AlumnoAddEditComponent } from "../alumnos/alumno-add-edit/alumno-add-edit.component";
import { AlumnoViewComponent } from "../alumnos/alumno-view/alumno-view.component";

@Component({
  selector: "app-inscripciones",
  templateUrl: "./inscripciones.component.html",
  styleUrls: ["./inscripciones.component.css"]
})
export class InscripcionesComponent implements OnInit {
  displayedColumns: string[] = [
    "ID",
    "Nombre",
    "Apellidos",
    "N. de Telefono",
    "Estado",
    "Acciones"
  ];
  dataSource: MatTableDataSource<any>;
  disabled: boolean = false;
  searchKey: string;
  dialogConfig = new MatDialogConfig();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private _alumnoService: AlumnoService,
    private _notificationService: NotificationService
  ) {
    this.configDialog();
  }

  async ngOnInit() {
    await this.loadTable();
  }

  async loadTable() {
    this._alumnoService.getAlumnos().subscribe(snap => {
      let array = snap.map(data => {
        let o = data.payload.doc.data() as any;
        return {
          fakeId: o.fakeId,
          id: data.payload.doc.id,
          nombre: o.nombre,
          primerApellido: o.primerApellido,
          segundoApellido: o.segundoApellido,
          n_telefono: o.n_telefono,
          estado: o.deleted ? "Inactivo" : "Activo",
          status: o.deleted
        };
      });

      this.dataSource = new MatTableDataSource(array);
      this.dataSource.paginator = this.paginator;
    });
  }

  onCreate() {
    this._alumnoService.form.reset();
    this.dialog.open(AlumnoAddEditComponent, this.dialogConfig);
  }

  onLaunch(id: string) {
    let dialogRef: MatDialogRef<AlumnoViewComponent> = this.dialog.open(
      AlumnoViewComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id = id;
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  configDialog(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = "60%";
    this.dialogConfig.height = "70%";
  }

  changeStatus(id: string, status: boolean) {
    let con = confirm("¿Esta seguro de cambiar el estado?");
    if (con) {
      this._alumnoService.changeStatus(id, status).subscribe(
        z => {
          this._notificationService.success("El estado fue cambiado");
        },
        err => {
          this._notificationService.warn("Error");
        }
      );
    }
  }
}
