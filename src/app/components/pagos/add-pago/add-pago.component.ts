import { Component, OnInit } from "@angular/core";
import { PagoService } from "src/app/services/pago.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { TorneoService } from "src/app/services/torneo.service";
import { MatSnackBar, MatDialogRef } from "@angular/material";
import { MyErrorStateMatcher } from "src/app/utils/MyErrorStateMatcher";
import { AlumnoService } from "src/app/services/alumno.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-add-pago",
  templateUrl: "./add-pago.component.html",
  styleUrls: ["./add-pago.component.css"]
})
export class AddPagoComponent implements OnInit {
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  alumnos: Observable<any>;
  loading: boolean = false;
  isEdited: boolean = false;
  id: string = "";

  constructor(
    private service: PagoService,
    private _alumno: AlumnoService,
    private _notificationService: NotificationService,
    private _snack: MatSnackBar,
    public dialogRef: MatDialogRef<AddPagoComponent>
  ) {}

  async ngOnInit() {
    if (this.isEdited) {
      this.service.getPago(this.id).subscribe(({ data }) => {
        this.service.setFormData((data as any).getPago);
      });
    } else {
      let today = new Date();
      let dd = String(today.getDate()).padStart(2, "0");
      let mm = String(today.getMonth() + 1).padStart(2, "0");
      let yyyy = today.getFullYear();

      this.service.form.controls.fecha.setValue(`${dd}-${mm}-${yyyy}`);
    }

    await this.getAlumnos();
  }

  async getAlumnos() {
    this.alumnos = this._alumno.getAlumnos();
  }

  onClose() {
    this.service.form.reset();
    this.dialogRef.close();
  }

  onSubmit() {
    if (!this.isEdited) {
      this.loading = true;
      this.service.registerPago(this.service.form.value).subscribe(
        ({ data }) => {
          this._notificationService.success("Pago realizado correctamente");
          this.service.form.reset();
          this.dialogRef.close();
        },
        err => {
          this.loading = false;
          this._snack.open("El pago no se realizo", "Error", {
            duration: 2500
          });
        }
      );
    } else {
      this.loading = true;
      this.service
        .updatePago({ id: this.id, ...this.service.form.value })
        .subscribe(
          ({ data }) => {
            this._notificationService.success("Pago editado correctamente");
            this.service.form.reset();
            this.dialogRef.close();
          },
          err => {
            this.loading = false;
            this._snack.open("Error al editar", "Error", {
              duration: 2500
            });
          }
        );
    }
  }
}
