import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialogConfig,
  MatDialog,
  MatTableDataSource,
  MatPaginator,
  MatPaginatorBase,
  MatDialogRef
} from "@angular/material";
import { PagoService } from "src/app/services/pago.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { AddPagoComponent } from "./add-pago/add-pago.component";
import { AlumnoService } from "src/app/services/alumno.service";

@Component({
  selector: "app-pagos",
  templateUrl: "./pagos.component.html",
  styleUrls: ["./pagos.component.css"]
})
export class PagosComponent implements OnInit {
  dialogConfig = new MatDialogConfig();
  displayedColumns: string[] = [
    "Alumno",
    "Fecha",
    "Concepto",
    "Monto",
    "Acciones"
  ];
  dataSource: MatTableDataSource<any>;
  searchKey: string = "";

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private service: PagoService,
    private _alumno: AlumnoService,
    private _notificationService: NotificationService
  ) {
    this.configDialog();
  }

  async ngOnInit() {
    await this.loadTable();
  }

  async loadTable() {
    this.service.getPagos().subscribe(snap => {
      let array = snap.map(data => {
        return { id: data.payload.doc.id, ...data.payload.doc.data() };
      });

      this.dataSource = new MatTableDataSource(array);
      this.dataSource.paginator = this.paginator;
    });
  }

  onCreate() {
    this.dialog.open(AddPagoComponent, this.dialogConfig);
  }

  configDialog(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = "40%";
    this.dialogConfig.height = "50%";
  }

  onEdit(id: string) {
    let dialogRef: MatDialogRef<AddPagoComponent> = this.dialog.open(
      AddPagoComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.isEdited = true;
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
}
