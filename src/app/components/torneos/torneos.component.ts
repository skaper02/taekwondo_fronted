import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatDialogConfig,
  MatPaginator,
  MatDialog,
  MatDialogRef
} from "@angular/material";
import { TorneoService } from "src/app/services/torneo.service";
import { AddFormComponent } from "./add-form/add-form.component";
import { NotificationService } from "src/app/services/notificationService.service";
import { ViewTorneoComponent } from "./view-torneo/view-torneo.component";
import { AddAlumnoComponent } from "./add-alumno/add-alumno.component";

@Component({
  selector: "app-torneos",
  templateUrl: "./torneos.component.html",
  styleUrls: ["./torneos.component.css"]
})
export class TorneosComponent implements OnInit {
  displayedColumns: string[] = ["Nombre", "Lugar", "Fecha", "Hora", "Acciones"];
  dataSource: MatTableDataSource<any>;
  disabled: boolean = false;
  searchKey: string;
  dialogConfig = new MatDialogConfig();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  //
  displayedColumnsHistory: string[] = [
    "Nombre",
    "Lugar",
    "Fecha",
    "Hora",
    "Acciones"
  ];
  dataSourceHistory: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginatorHistory: MatPaginator;

  constructor(
    private _torneo: TorneoService,
    private dialog: MatDialog,
    private _notification: NotificationService
  ) {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = "45%";
    this.dialogConfig.height = "45%";
  }

  async ngOnInit() {
    await this.loadTable();
    await this.loadTableHistory();
  }

  async loadTable() {
    this._torneo.getTorneos().subscribe(s => {
      let arr = s
        .filter((data: any) => !data.payload.doc.data().deleted)
        .map(data => {
          let o = data.payload.doc.data() as any;
          return {
            id: data.payload.doc.id,
            nombre: o.nombre,
            lugar: o.lugar,
            fecha: o.fecha.substring(0, 10),
            hora: o.hora + " Hrs.",
            deleted: o.deleted
          };
        });

      this.dataSource = new MatTableDataSource(arr);
      this.dataSource.paginator = this.paginator;
    });
  }

  async loadTableHistory() {
    this._torneo.getTorneos().subscribe(s => {
      let arr = s
        .filter((data: any) => data.payload.doc.data().deleted)
        .map(data => {
          let o = data.payload.doc.data() as any;
          return {
            id: data.payload.doc.id,
            nombre: o.nombre,
            lugar: o.lugar,
            fecha: o.fecha.substring(0, 10),
            hora: o.hora + " Hrs.",
            deleted: o.deleted
          };
        });

      this.dataSourceHistory = new MatTableDataSource(arr);
      // this.dataSourceHistory.paginator = this.paginatorHistory;
    });
  }

  onCreate() {
    this.dialogConfig.width = "45%";
    this.dialogConfig.height = "45%";

    this.dialog.open(AddFormComponent, this.dialogConfig);
  }

  onLaunch(id: string, deleted: boolean) {
    this.dialogConfig.width = "75%";
    this.dialogConfig.height = "75%";
    let dialogRef: MatDialogRef<ViewTorneoComponent> = this.dialog.open(
      ViewTorneoComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.isHistorial = deleted;
  }

  onEdit(id: string) {
    this.dialogConfig.width = "45%";
    this.dialogConfig.height = "45%";

    let dialogRef: MatDialogRef<AddFormComponent> = this.dialog.open(
      AddFormComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.isEdited = true;
  }

  onDelete(id: string) {
    let res = confirm("¿Esta seguro de eliminar este torneo?");
    if (res) {
      this._torneo.deleteTorneo(id).subscribe(
        ({ data }) => {
          this._notification.success("Torneo eliminado");
        },
        e => {
          this._notification.warn("Error inesperado");
        }
      );
    }
  }

  onAdd(id: string) {
    this.dialogConfig.width = "60%";
    this.dialogConfig.height = "50%";

    let dialogRef: MatDialogRef<AddAlumnoComponent> = this.dialog.open(
      AddAlumnoComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id_torneo = id;
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
}
