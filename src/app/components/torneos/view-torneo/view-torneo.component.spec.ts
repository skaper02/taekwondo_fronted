import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTorneoComponent } from './view-torneo.component';

describe('ViewTorneoComponent', () => {
  let component: ViewTorneoComponent;
  let fixture: ComponentFixture<ViewTorneoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTorneoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTorneoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
