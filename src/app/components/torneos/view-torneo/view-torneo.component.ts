import { Component, OnInit, ViewChild } from "@angular/core";
import { TorneoService } from "src/app/services/torneo.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { AddFormComponent } from "../add-form/add-form.component";
import {
  MatDialogRef,
  MatTableDataSource,
  MatPaginator
} from "@angular/material";

@Component({
  selector: "app-view-torneo",
  templateUrl: "./view-torneo.component.html",
  styleUrls: ["./view-torneo.component.css"]
})
export class ViewTorneoComponent implements OnInit {
  displayedColumns: string[] = [
    "ID",
    "Nombre",
    "Grado",
    "Telefono",
    "Acciones"
  ];

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  id: string;
  showSpinner: boolean = false;
  isHistorial: boolean = false;
  constructor(
    public service: TorneoService,
    private _notificationService: NotificationService,
    public dialogRef: MatDialogRef<AddFormComponent>
  ) {}

  async ngOnInit() {
    console.log(this.isHistorial);
    this.showSpinner = true;

    await this.loadTable();

    this.service.getTorneo(this.id).subscribe(
      ({ data }) => {
        console.log(data);

        this.showSpinner = false;
        this.service.setFormData((data as any).getTorneo);
      },
      err => {
        this.showSpinner = false;
      }
    );
  }

  async loadTable() {
    this.service.getAlumnosPerTorneo(this.id).subscribe(({ data }) => {
      this.dataSource = new MatTableDataSource(
        (data as any).getAlumnosPerTorneo
      );
      this.dataSource.paginator = this.paginator;
    });
  }

  onDelete(id: string) {
    this.showSpinner = true;
    let res = confirm("¿Esta seguro de eliminar este alumno del torneo?");
    if (res) {
      this.service.deleteAlumnoTorneo(this.id, id).subscribe(
        ({ data }) => {
          this.showSpinner = false;
          this._notificationService.success("Eliminado correctamente");

          this.loadTable();
        },
        err => {
          this.showSpinner = false;
          this._notificationService.warn("Error");
        }
      );
    }
  }

  onClose() {
    this.isHistorial = false;
    this.dialogRef.close();
  }

  grado(grd: string) {
    switch (grd) {
      case "1":
        return "10° KUP - Blanco";
      case "2":
        return "9° KUP - Blanca avanzada";
      case "3":
        return "8º KUP - Naranja";
      case "4":
        return "7º KUP - Naranja Avanzada";
      case "5":
        return "6º KUP - Verde";
      case "6":
        return "5º KUP - Verde Avanzada";
      case "7":
        return "4º KUP - Azul";
      case "8":
        return "3º KUP - Azul Avanzada";
      case "9":
        return "2º KUP - Rojo";
      case "10":
        return "1º KUP - Rojo Avanzada";
      case "11":
        return "1º DAN o POOM - Negro";
      default:
        break;
    }
  }
}
