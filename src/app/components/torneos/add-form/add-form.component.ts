import { Component, OnInit } from "@angular/core";
import { MyErrorStateMatcher } from "src/app/utils/MyErrorStateMatcher";
import { Observable } from "rxjs";
import { TorneoService } from "src/app/services/torneo.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { MatDialogRef, MatSnackBar } from "@angular/material";

@Component({
  selector: "app-add-form",
  templateUrl: "./add-form.component.html",
  styleUrls: ["./add-form.component.css"]
})
export class AddFormComponent implements OnInit {
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  isEdited: boolean = false;
  showSpinner: boolean = false;
  id: string;
  data: Observable<any>;

  constructor(
    public service: TorneoService,
    private _notificationService: NotificationService,
    public dialogRef: MatDialogRef<AddFormComponent>,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    if (this.isEdited) {
      this.showSpinner = true;

      this.service.getTorneo(this.id).subscribe(
        ({ data }) => {
          this.showSpinner = false;
          this.service.setFormData((data as any).getTorneo);
        },
        err => {
          this.showSpinner = false;
        }
      );
    }
  }

  onClose() {
    this.service.form.reset();
    this.dialogRef.close();
  }

  onSubmit() {
    if (!this.isEdited) this.register();
    else this.edit(this.id);
  }

  edit(id: string) {
    this.showSpinner = true;
    this.service.editTorneo(this.service.form.value, id).subscribe(
      ({ data }) => {
        this.showSpinner = false;

        this._notificationService.success("Editado correctamente");
        // exit
        this.service.form.reset();
        this.dialogRef.close();
      },
      err => {
        this.showSpinner = false;
        this._notificationService.warn("Hubo un problema");
      }
    );
  }

  register() {
    this.showSpinner = true;
    this.service
      .registerTorneo(this.service.form.value)
      .subscribe(({ data }) => {
        this.showSpinner = false;
        this._notificationService.success("Agregado correctamente");
        // exit
        this.service.form.reset();
        this.dialogRef.close();
      });
  }
}
