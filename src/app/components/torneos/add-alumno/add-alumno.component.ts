import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatDialogConfig,
  MatPaginator,
  MatDialog,
  MatDialogRef,
  MatSnackBar
} from "@angular/material";
import { AlumnoService } from "src/app/services/alumno.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { TorneoService } from "src/app/services/torneo.service";

@Component({
  selector: "app-add-alumno",
  templateUrl: "./add-alumno.component.html",
  styleUrls: ["./add-alumno.component.css"]
})
export class AddAlumnoComponent implements OnInit {
  displayedColumns: string[] = [
    "ID",
    "Nombre",
    "Apellidos",
    "N. de Telefono",
    "Acciones"
  ];
  dataSource: MatTableDataSource<any>;
  searchKey: string;
  dialogConfig = new MatDialogConfig();
  id_torneo: string;
  showSpinner: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private _alumnoService: AlumnoService,
    private _notificationService: NotificationService,
    private torneoService: TorneoService,
    private _snack: MatSnackBar,
    public dialogRef: MatDialogRef<AddAlumnoComponent>
  ) {}

  async ngOnInit() {
    await this.loadTable();
  }

  async loadTable() {
    this._alumnoService.getAlumnos().subscribe(snap => {
      let array = snap
        .filter((data: any) => !data.payload.doc.data().deleted)
        .map(data => {
          let o = data.payload.doc.data() as any;
          return {
            fakeId: o.fakeId,
            id: data.payload.doc.id,
            nombre: o.nombre,
            primerApellido: o.primerApellido,
            segundoApellido: o.segundoApellido,
            n_telefono: o.n_telefono
          };
        });

      this.dataSource = new MatTableDataSource(array);
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  onClose() {
    this.dialogRef.close();
  }

  onAdd(id: string) {
    this.showSpinner = true;
    this.torneoService.addAlumnoTorneo(this.id_torneo, id).subscribe(
      ({ data }) => {
        this.showSpinner = false;
        if (data.addAlumnoTorneo.status == "success") {
          this._notificationService.success("Agregado correctamente");
        } else if (data.addAlumnoTorneo.status == "warn") {
          this._snack.open("Este alumno ya esta registrado", "Error", {
            duration: 2500
          });
        }
      },
      err => {
        this.showSpinner = false;
        this._notificationService.warn("Error");
      }
    );
  }
}
