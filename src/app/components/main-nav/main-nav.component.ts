import { Component, Inject } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AuthService } from "src/app/services/auth.service";
import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material";
import { InstructoresService } from "src/app/services/instructores.service";
import { InstructoresViewComponent } from "../instructores/instructores-view/instructores-view.component";
import * as firebase from "firebase";
import { InstructoresComponent } from "../instructores/instructores/instructores.component";

@Component({
  selector: "app-main-nav",
  templateUrl: "./main-nav.component.html",
  styleUrls: ["./main-nav.component.css"]
})
export class MainNavComponent {
  hasToken = localStorage.getItem("token");
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private breakpointObserver: BreakpointObserver,
    private _auth: AuthService,
    private dialog: MatDialog
  ) {}

  async logout() {
    let res = confirm("¿Esta seguro de salir?");
    if (res) {
      await this._auth.logoutToken();
      this.hasToken = null;
      this._auth.logoutUser();
    }
  }

  aboutMe() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.height = "70%";

    this.dialog.open(InstructoresViewComponent, dialogConfig);
  }

  edit() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.height = "70%";
    let dialogRef = this.dialog.open(InstructoresComponent, dialogConfig);
    // set isEdited property to true and set the id to edit
    dialogRef.componentInstance.isEdited = true;
    dialogRef.componentInstance.id = this._auth.getCurrentUID();
  }
}
