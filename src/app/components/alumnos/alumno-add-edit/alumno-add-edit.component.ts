import { Component, OnInit } from "@angular/core";
import { AlumnoService } from "src/app/services/alumno.service";
import { NotificationService } from "src/app/services/notificationService.service";
import { MatDialogRef, MatSnackBar } from "@angular/material";
import { MyErrorStateMatcher } from "src/app/utils/MyErrorStateMatcher";
import { HorarioService } from "src/app/services/horario.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-alumno-add-edit",
  templateUrl: "./alumno-add-edit.component.html",
  styleUrls: ["./alumno-add-edit.component.css"]
})
export class AlumnoAddEditComponent implements OnInit {
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  isEdited: boolean = false;
  showSpinner: boolean = false;
  id: string;
  data: Observable<any>;

  // dates
  public minDate: Date;
  public maxDate: Date;

  constructor(
    public service: AlumnoService,
    private _notificationService: NotificationService,
    public dialogRef: MatDialogRef<AlumnoAddEditComponent>,
    private snackBar: MatSnackBar,
    private _hoario: HorarioService
  ) {
    // max age
    this.maxDate = new Date(new Date().getFullYear() - 5, 0, 1);
    // min age
    this.minDate = new Date(new Date().getFullYear() - 60, 0, 1);
  }

  ngOnInit() {
    // load clases
    this.data = this._hoario.getAllClases();

    if (this.isEdited) {
      this.showSpinner = true;

      this.service.getAlumno(this.id).subscribe(
        ({ data }) => {
          this.showSpinner = false;
          this.service.setFormData((data as any).getAlumno);
        },
        err => {
          this.showSpinner = false;
        }
      );
    }
  }

  onSubmit() {
    if (!this.isEdited) this.register();
    else this.edit(this.id);
  }

  register() {
    this.showSpinner = true;
    this.service.registerAlumno(this.service.form.value).subscribe(
      ({ data }) => {
        if (data.registerAlumno.status == "Error") {
          this.showSpinner = false;
          this.snackBar.open(
            data.registerAlumno.message,
            data.registerAlumno.status,
            {
              duration: 2500
            }
          );
        } else {
          this.showSpinner = false;
          this._notificationService.success("Agregado correctamente");
          // exit
          this.service.form.reset();
          this.dialogRef.close();
        }
      },
      err => {
        this.showSpinner = false;
        this._notificationService.warn("Hubo un problema");
      }
    );
  }

  edit(id: string) {
    this.showSpinner = true;
    this.service.editAlumno(this.service.form.value, id).subscribe(
      ({ data }) => {
        if (data.editAlumno.status == "Error") {
          this.showSpinner = false;
          this.snackBar.open(data.editAlumno.message, data.editAlumno.status, {
            duration: 2500
          });
        } else {
          this.showSpinner = false;
          this._notificationService.success("Editado correctamente");
          // exit
          this.service.form.reset();
          this.dialogRef.close();
        }
      },
      err => {
        this.showSpinner = false;
        this._notificationService.warn("Hubo un problema");
      }
    );
  }

  onClose() {
    this.service.form.reset();
    this.dialogRef.close();
  }

  showSnack(msg1: string, msg2: string) {
    this.snackBar.open(msg1, msg2, {
      duration: 2500
    });
  }
}
