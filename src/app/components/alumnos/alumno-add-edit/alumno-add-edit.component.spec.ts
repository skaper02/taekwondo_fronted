import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumnoAddEditComponent } from './alumno-add-edit.component';

describe('AlumnoAddEditComponent', () => {
  let component: AlumnoAddEditComponent;
  let fixture: ComponentFixture<AlumnoAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlumnoAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlumnoAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
