import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialogConfig,
  MatDialogRef,
  MatDialog
} from "@angular/material";
import { NotificationService } from "src/app/services/notificationService.service";
import { AlumnoService } from "src/app/services/alumno.service";
import { AlumnoViewComponent } from "../alumno-view/alumno-view.component";
import { AlumnoAddEditComponent } from "../alumno-add-edit/alumno-add-edit.component";
import * as jsPDF from "jspdf";

@Component({
  selector: "app-alumnos",
  templateUrl: "./alumnos.component.html",
  styleUrls: ["./alumnos.component.css"]
})
export class AlumnosComponent implements OnInit {
  displayedColumns: string[] = [
    "ID",
    "Nombre",
    "Apellidos",
    "N. de Telefono",
    "Acciones"
  ];
  dataSource: MatTableDataSource<any>;
  instructores;
  disabled: boolean = false;
  searchKey: string;
  dialogConfig = new MatDialogConfig();
  selected = "0";

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private _alumnoService: AlumnoService,
    private _notificationService: NotificationService
  ) {
    this.configDialog();
  }

  async ngOnInit() {
    await this.loadTable();
  }

  async loadTable() {
    this._alumnoService.getAlumnos().subscribe(snap => {
      let array = snap
        .filter((data: any) => !data.payload.doc.data().deleted)
        .map(data => {
          let o = data.payload.doc.data() as any;
          return {
            fakeId: o.fakeId,
            id: data.payload.doc.id,
            nombre: o.nombre,
            primerApellido: o.primerApellido,
            segundoApellido: o.segundoApellido,
            n_telefono: o.n_telefono
          };
        });

      this.dataSource = new MatTableDataSource(array);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  onCreate() {
    this.dialog.open(AlumnoAddEditComponent, this.dialogConfig);
  }

  onLaunch(id: string) {
    let dialogRef: MatDialogRef<AlumnoViewComponent> = this.dialog.open(
      AlumnoViewComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id = id;
  }

  onEdit(id: string) {
    let dialogRef: MatDialogRef<AlumnoAddEditComponent> = this.dialog.open(
      AlumnoAddEditComponent,
      this.dialogConfig
    );

    // set id property
    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.isEdited = true;
  }

  onDelete(id: string) {
    let res = confirm("¿Esta seguro de eliminar este alumno?");
    if (res) {
      this._alumnoService.deleteAlumno(id).subscribe(
        ({ data }) => {
          this._notificationService.success("Eliminado Correctamente");
        },
        err => {
          console.log(err);

          this._notificationService.warn("Hubo un error");
        }
      );
    }
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  configDialog(): void {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = "60%";
    this.dialogConfig.height = "70%";
  }

  toPrint() {
    this._alumnoService
      .getAlumnoPerGrado(this.selected)
      .subscribe(({ data }) => {
        let array = (data as any).getAlumnosPerGrado.map(data => {
          let o = data as any;
          return {
            id: o.fakeId + "",
            nombre:
              o.nombre +
              " " +
              o.primerApellido +
              " " +
              (o.segundoApellido || ""),
            grado: this.grado(o.grado),
            telefono: o.n_telefono
          };
        });

        let headers = [
          {
            id: "id",
            name: "id",
            prompt: "ID",
            width: 30
          },
          {
            id: "nombre",
            name: "nombre",
            prompt: "Nombre",
            width: 80
          },
          {
            id: "grado",
            name: "grado",
            prompt: "Grado",
            width: 80
          },
          {
            id: "telefono",
            name: "telefono",
            prompt: "Teléfono",
            width: 60
          }
        ] as string[] | any;

        // date
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, "0");
        let mm = String(today.getMonth() + 1).padStart(2, "0");
        let yyyy = today.getFullYear();

        //
        // ─── PDF CONFIGURATION ──────────────────────────────────────────────────────────
        //
        var doc = new jsPDF({
          putOnlyUsedFonts: true,
          orientation: "landscape"
        });
        let img = new Image();
        img.src = "assets/o.png";

        doc.addImage(img, "png", 5, 5, 40, 40);
        doc.setFontSize(20);
        doc.text("Reporte de alumnos", 50, 10);
        doc.setFontSize(14);
        doc.text("San Pablo Huitzo", 50, 18);
        doc.text("Taekwondo Huitzo", 50, 23);
        doc.text(`Fecha: ${dd}-${mm}-${yyyy}`, 50, 28);
        doc.table(5, 50, array, headers, { autoSize: false });
        doc.save("reporte_alumnos");
      });
  }

  grado(grd: string) {
    switch (grd) {
      case "1":
        return "10° KUP - Blanco";
      case "2":
        return "9° KUP - Blanca avanzada";
      case "3":
        return "8º KUP - Naranja";
      case "4":
        return "7º KUP - Naranja Avanzada";
      case "5":
        return "6º KUP - Verde";
      case "6":
        return "5º KUP - Verde Avanzada";
      case "7":
        return "4º KUP - Azul";
      case "8":
        return "3º KUP - Azul Avanzada";
      case "9":
        return "2º KUP - Rojo";
      case "10":
        return "1º KUP - Rojo Avanzada";
      case "11":
        return "1º DAN o POOM - Negro";
      default:
        break;
    }
  }
}
