import { Component, OnInit } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { AlumnoService } from "src/app/services/alumno.service";
import { Observable } from "rxjs";
import { HorarioService } from "src/app/services/horario.service";

@Component({
  selector: "app-alumno-view",
  templateUrl: "./alumno-view.component.html",
  styleUrls: ["./alumno-view.component.css"]
})
export class AlumnoViewComponent implements OnInit {
  id: string;
  showSpinner: boolean = false;
  data: Observable<any>;

  constructor(
    private dialogRef: MatDialogRef<AlumnoViewComponent>,
    private service: AlumnoService,
    private _horario: HorarioService
  ) {}

  ngOnInit() {
    // load clases
    this.data = this._horario.getAllClases();

    this.showSpinner = true;
    this.service.getAlumno(this.id).subscribe(({ data }) => {
      this.showSpinner = false;

      this.service.setFormData((data as any).getAlumno);
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  tipo_asentamiento(t: string) {
    switch (t) {
      case "1":
        return "Colonia";
      case "2":
        return "Fraccionamiento";
      case "3":
        return "Barrio";
      case "4":
        return "Rancheria";
      default:
        break;
    }
  }

  municipio(mncp: string) {
    switch (mncp) {
      case "1":
        return "Santiago Suchilquitongo";
      case "2":
        return "San Francisco Telixtlahuaca";
      case "3":
        return "San Pablo Huitzo";
      case "4":
        return "Magdalena Apasco";
      default:
        break;
    }
  }

  grado(grd: string) {
    switch (grd) {
      case "1":
        return "10° KUP - Blanco";
      case "2":
        return "9° KUP - Blanca avanzada";
      case "3":
        return "8º KUP - Naranja";
      case "4":
        return "7º KUP - Naranja Avanzada";
      case "5":
        return "6º KUP - Verde";
      case "6":
        return "5º KUP - Verde Avanzada";
      case "7":
        return "4º KUP - Azul";
      case "8":
        return "3º KUP - Azul Avanzada";
      case "9":
        return "2º KUP - Rojo";
      case "10":
        return "1º KUP - Rojo Avanzada";
      case "11":
        return "1º DAN o POOM - Negro";
      default:
        break;
    }
  }
}
