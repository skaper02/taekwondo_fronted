import { Component, OnInit, ViewChild } from "@angular/core";
import { InstructoresService } from "src/app/services/instructores.service";
import { HorarioService } from "src/app/services/horario.service";
import { Observable } from "rxjs";
import {
  MatDialogRef,
  MatTableDataSource,
  MatPaginator
} from "@angular/material";

@Component({
  selector: "app-view-horario",
  templateUrl: "./view-horario.component.html",
  styleUrls: ["./view-horario.component.css"]
})
export class ViewHorarioComponent implements OnInit {
  id: string = "";
  loading: boolean = false;
  data: Observable<any>;
  horarioObs: Observable<any>;

  // table
  displayedColumns: string[] = ["id", "nombre", "grado"];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private _instructores: InstructoresService,
    private _horario: HorarioService,
    private ref: MatDialogRef<ViewHorarioComponent>
  ) {
    this.data = this._instructores.instructorDataTable();
  }

  ngOnInit() {
    this.loading = true;
    this._horario.getClase(this.id).subscribe(({ data }) => {
      this.loading = false;
      this._horario.setFormData((data as any).getClase);
    });

    this._horario.getAlumnosPerClase(this.id).subscribe(({ data }) => {
      console.log(data as []);

      this.dataSource = new MatTableDataSource(
        (data as any).getAlumnosPerClase
      );
      this.dataSource.paginator = this.paginator;
    });
  }

  onClose() {
    this.ref.close();
    this._horario.form.reset();
  }

  grado(grd: string) {
    switch (grd) {
      case "1":
        return "10° KUP - Blanco";
      case "2":
        return "9° KUP - Blanca avanzada";
      case "3":
        return "8º KUP - Naranja";
      case "4":
        return "7º KUP - Naranja Avanzada";
      case "5":
        return "6º KUP - Verde";
      case "6":
        return "5º KUP - Verde Avanzada";
      case "7":
        return "4º KUP - Azul";
      case "8":
        return "3º KUP - Azul Avanzada";
      case "9":
        return "2º KUP - Rojo";
      case "10":
        return "1º KUP - Rojo Avanzada";
      case "11":
        return "1º DAN o POOM - Negro";
      default:
        break;
    }
  }
}
