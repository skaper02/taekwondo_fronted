import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewHorarioComponent } from './view-horario.component';

describe('ViewHorarioComponent', () => {
  let component: ViewHorarioComponent;
  let fixture: ComponentFixture<ViewHorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewHorarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewHorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
