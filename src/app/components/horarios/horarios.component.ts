import { Component, OnInit, ViewChild } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatTableDataSource,
  MatPaginator,
  MatDialogRef,
  MatSnackBar
} from "@angular/material";
import { AddHorarioComponent } from "./add-horario/add-horario.component";
import { HorarioService } from "src/app/services/horario.service";
import { InstructoresService } from "src/app/services/instructores.service";
import { ViewHorarioComponent } from "./view-horario/view-horario.component";
import { NotificationService } from "src/app/services/notificationService.service";

@Component({
  selector: "app-horarios",
  templateUrl: "./horarios.component.html",
  styleUrls: ["./horarios.component.css"]
})
export class HorariosComponent implements OnInit {
  displayedColumns: string[] = ["inicio", "fin", "instructor", "acciones"];
  dataSource: MatTableDataSource<any>;
  clases: any;
  keyString: string = "";
  dialogConfig = new MatDialogConfig();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private _horario: HorarioService,
    private _notification: NotificationService,
    private _mat: MatSnackBar
  ) {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.autoFocus = true;
    this.dialogConfig.width = "40%";
    this.dialogConfig.height = "60%";
  }

  ngOnInit() {
    this.loadTable();
  }

  loadTable() {
    this._horario.getClases().subscribe(s => {
      let a = s
        .map(d => {
          let o = d.payload.doc.data() as any;

          return {
            id: d.payload.doc.id,
            inicio: o.hora_inicio,
            fin: o.hora_termino,
            instructor:
              o.nombre + " " + o.primerApellido + " " + o.segundoApellido,
            deleted: o.deleted
          };
        })
        .filter(x => !x.deleted);
      this.dataSource = new MatTableDataSource(a);
      this.dataSource.paginator = this.paginator;
    });
  }

  onCreate() {
    this.dialogConfig.width = "40%";
    this.dialogConfig.height = "60%";

    this.dialog.open(AddHorarioComponent, this.dialogConfig);
  }

  applyFilter() {
    this.dataSource.filter = this.keyString.trim().toLocaleLowerCase();
  }

  onSearchClear() {
    this.keyString = "";
    this.applyFilter();
  }

  onLaunch(id: string) {
    this.dialogConfig.width = "60%";
    this.dialogConfig.height = "60%";
    let dialogRef: MatDialogRef<ViewHorarioComponent> = this.dialog.open(
      ViewHorarioComponent,
      this.dialogConfig
    );
    dialogRef.componentInstance.id = id;
  }

  onEdit(id: string) {
    this.dialogConfig.width = "40%";
    this.dialogConfig.height = "60%";

    let dialogRef: MatDialogRef<AddHorarioComponent> = this.dialog.open(
      AddHorarioComponent,
      this.dialogConfig
    );

    dialogRef.componentInstance.id = id;
    dialogRef.componentInstance.isEdit = true;
  }

  onDelete(id: string) {
    let res = confirm(
      "¿Esta seguro de eliminar esta clase?\nSi hay alumnos inscritos se les reasignara su clase"
    );

    if (res) {
      this._horario.deleteClase(id).subscribe(
        ({ data }) => {
          // this._notification.success("Clase eliminada");
          if (data.deleteClase.status == "warn") {
            this._mat.open(data.deleteClase.message, "Error", {
              duration: 2500
            });
          } else {
            this._mat.open(data.deleteClase.message, "Clase eliminada", {
              duration: 2500
            });
          }
        },
        e => {
          this._notification.warn("Hubo un error");
        }
      );
    }
  }
}
