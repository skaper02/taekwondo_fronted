import { Component, OnInit } from "@angular/core";
import { HorarioService } from "src/app/services/horario.service";
import { MatDialogRef, MatSnackBar } from "@angular/material";
import { MyErrorStateMatcher } from "src/app/utils/MyErrorStateMatcher";
import { InstructoresService } from "src/app/services/instructores.service";
import { Observable } from "rxjs";
import { NotificationService } from "src/app/services/notificationService.service";

@Component({
  selector: "app-add-horario",
  templateUrl: "./add-horario.component.html",
  styleUrls: ["./add-horario.component.css"]
})
export class AddHorarioComponent implements OnInit {
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  instructores: any;
  data: Observable<any>;
  loading: boolean = false;
  isEdit: boolean = false;
  id: string = "";

  //
  // ─── GET DATA ───────────────────────────────────────────────────────────────────
  //

  horarioObs: Observable<any>;

  constructor(
    public _horario: HorarioService,
    private _instructors: InstructoresService,
    private ref: MatDialogRef<AddHorarioComponent>,
    private _notification: NotificationService,
    private snack: MatSnackBar
  ) {}

  ngOnInit() {
    if (this.isEdit) this.showData();
    this.data = this._instructors.instructorDataTable();
  }

  showData() {
    this.loading = true;
    this._horario.getClase(this.id).subscribe(({ data }) => {
      this.loading = false;
      this._horario.setFormData((data as any).getClase);
    });
  }

  onClose() {
    this._horario.form.reset();
    this.ref.close();
    this.isEdit = false;
  }

  onSubmit() {
    this.loading = true;
    if (this.isEdit) this.edit();
    else {
      this._horario
        .addClase(this._horario.form.value)
        .toPromise()
        .then(({ data }) => {
          if (data.addClase.status == "success") {
            this._notification.success("Clase agregada");
            this._horario.form.reset();
            this.ref.close();
          } else {
            this.snack.open(data.addClase.message, "Error", {
              duration: 2500
            });
          }
        })
        .catch(e => this._notification.warn("Error"))
        .finally(() => (this.loading = false));
    }
  }

  edit() {
    this._horario
      .editClase({ ...this._horario.form.value, id: this.id })
      .toPromise()
      .then(({ data }) => {
        if (data.editClase.status == "success") {
          this._notification.success("Clase editada");
          this._horario.form.reset();
          this.ref.close();
          this.isEdit = false;
        } else {
          this.snack.open(data.editClase.message, "Error", {
            duration: 2500
          });
        }
      })
      .catch(e => {
        console.log(e);
        this._notification.warn("Error");
      })
      .finally(() => {
        this.loading = false;
      });
  }
}
