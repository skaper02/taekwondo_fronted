import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { _MatBadgeMixinBase, MatSnackBar } from '@angular/material';
import { MyErrorStateMatcher } from 'src/app/utils/MyErrorStateMatcher';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import * as firebase from 'firebase';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  loginForm: FormGroup;
  showSpinner: boolean = false;

  constructor(
    private backenServive: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.loginForm = new FormGroup({
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      passwordFormControl: new FormControl('', [Validators.required])
    });
    try {
      firebase.initializeApp(environment.firebaseConfig);
    } catch (e) {}
  }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.router.navigate(['dashboard/instructores']);
    }
  }

  async loginUser(): Promise<void> {
    if (this.loginForm.valid) {
      this.showSpinner = true;
      try {
        this.backenServive
          .isVerifiedEmail(this.loginForm.value.emailFormControl)
          .subscribe(
            async ({ data }) => {
              const p = data as any;
              if (p.emailVerified) {
                await this.ifIsLogin();
              } else {
                this.showSpinner = false;
                this.showSnackBarMsg('Correo no verificado', 'Verifique');
              }
            },
            err => {
              this.showSpinner = false;
              const error = (err as any).graphQLErrors.map(x => x.message)[0];
              this.showSnackBarMsg(error, 'Verifique');
            }
          );
      } catch (e) {
        this.showSnackBarMsg('Verifique sus datos', 'Verifique');
      }
    }
  }

  // show snackbar with 2500 duration
  showSnackBarMsg(msg1: string, msg2: string) {
    this.snackBar.open(msg1, msg2, {
      duration: 2500
    });
  }

  // store the token is user is login
  async ifIsLogin() {
    try {
      await this.backenServive.login(this.loginForm);
      const value = await this.backenServive.getCurrentId();
      window.localStorage.setItem('token', value);

      this.showSpinner = false;
      this.router.navigate(['dashboard/instructores']);
    } catch (e) {
      this.showSpinner = false;
      this.snackBar.open('Datos invalidos', 'verifique', {
        duration: 2500
      });
    }
  }

  // send link to restore password
  async forgetPass() {
    const email = prompt('Ingrese su correo', '');
    if (email == null) {
      this.showSpinner = false;
    } else {
      await this.backenServive.forgetPassword(email).subscribe(
        ({ data }) => {
          this.snackBar.open('Ya puede reestablecer su contrasena', '', {
            duration: 2500
          });
        },
        e => {
          this.snackBar.open('La direccion de correo no existe', 'verifique', {
            duration: 2500
          });
        }
      );
    }
  }
}
