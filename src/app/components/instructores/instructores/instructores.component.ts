import { Component, OnInit, Input } from "@angular/core";
import { MyErrorStateMatcher } from "src/app/utils/MyErrorStateMatcher";
import { InstructoresService } from "src/app/services/instructores.service";
import { MatDialogRef, MatSnackBar } from "@angular/material";
import { NotificationService } from "src/app/services/notificationService.service";

@Component({
  selector: "app-instructores",
  templateUrl: "./instructores.component.html",
  styleUrls: ["./instructores.component.css"]
})
export class InstructoresComponent implements OnInit {
  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  public id: string;
  public isEdited: boolean;
  public minDate: Date;
  public maxDate: Date;
  showSpinner: boolean = false;

  constructor(
    private service: InstructoresService,
    public dialogRef: MatDialogRef<InstructoresComponent>,
    private snackBar: MatSnackBar,
    private notificationService: NotificationService
  ) {
    //max age
    this.maxDate = new Date(new Date().getFullYear() - 18, 0, 1);
    //min age
    this.minDate = new Date(new Date().getFullYear() - 60, 0, 1);
  }

  ngOnInit() {
    if (this.isEdited) {
      // enable load animation
      this.showSpinner = true;
      this.service.specificInstructor(this.id).subscribe(
        ({ data }) => {
          // disable load animation
          this.showSpinner = false;

          this.service.setData(data);
        },
        err => {
          this.showSpinner = false;
        }
      );
    }
  }

  onSubmit() {
    if (!this.isEdited) this.register();
    else this.edit(this.id);
  }

  onClose() {
    // Rest form and close popup
    this.service.form.reset();
    this.dialogRef.close();
  }

  edit(id: string) {
    this.showSpinner = true;
    this.service.editInstructor(this.service.form, id).subscribe(
      ({ data }) => {
        this.showSpinner = false;
        if (data.editInstructor.status === "Success") {
          this.notificationService.success("Editado correctamente");

          // Rest form and close popup
          this.service.form.reset();
          this.dialogRef.close();
        }
      },
      error => {
        this.showSpinner = false;
        // show what error is
        let err = (error as any).graphQLErrors.map(x => x.message)[0];
        this.snackBar.open(err, "Verifique", {
          duration: 2500
        });
      }
    );
  }

  register() {
    this.showSpinner = true;
    this.service.registerInstructor(this.service.form).subscribe(
      ({ data }) => {
        this.showSpinner = false;
        if (data.registerInstructor.status === "Success") {
          this.notificationService.success("Agregado correctamente");

          this.snackBar.open(
            "Se ha enviado informacion adicional a su correo",
            "Verifique",
            {
              duration: 2500
            }
          );

          // Rest form and close popup
          this.service.form.reset();
          this.dialogRef.close();
        }
      },
      error => {
        this.showSpinner = false;
        // show what error is
        let err = (error as any).graphQLErrors.map(x => x.message)[0];
        this.snackBar.open(err, "Verifique", {
          duration: 2500
        });
      }
    );
  }
}
