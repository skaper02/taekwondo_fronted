import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructoresViewComponent } from './instructores-view.component';

describe('InstructoresViewComponent', () => {
  let component: InstructoresViewComponent;
  let fixture: ComponentFixture<InstructoresViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructoresViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructoresViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
