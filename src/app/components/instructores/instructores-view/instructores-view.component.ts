import { Component, OnInit } from "@angular/core";
import { MyErrorStateMatcher } from "src/app/utils/MyErrorStateMatcher";
import { InstructoresService } from "src/app/services/instructores.service";
import {
  MatDialogRef,
  MatSnackBar,
  MatDialogConfig,
  MatDialog
} from "@angular/material";
import { InstructoresComponent } from "../instructores/instructores.component";
import { InstructorData } from "../instructores-list/instructores-list.component";
import { NotificationService } from "src/app/services/notificationService.service";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-instructores-view",
  templateUrl: "./instructores-view.component.html",
  styleUrls: ["./instructores-view.component.css"]
})
export class InstructoresViewComponent implements OnInit {
  public id: string;
  public grad: number;
  public tipo_as: number;
  public tipo: string;
  public mncp: string;
  showSpinner: boolean = false;

  constructor(
    private service: InstructoresService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<InstructoresViewComponent>,
    private notificationService: NotificationService,
    private _auth: AuthService
  ) {}

  ngOnInit() {
    // load animation
    this.showSpinner = true;
    // if id exists fetch data for an specific instructor
    if (this.id) {
      this.service.specificInstructor(this.id).subscribe(({ data }) => {
        // disable load animation
        this.showSpinner = false;
        this.service.setData(data);
        const ignoreType = (data as any).instructor;
        this.tipo_as = ignoreType.tipo_asentamiento;
        this.grad = ignoreType.grado;
        this.mncp = ignoreType.municipio;
      });
    } else {
      // fetch current session data
      this.service.me().subscribe(({ data }) => {
        // disable load animation
        this.showSpinner = false;
        this.service.setMeData(data);
        const ignoreType = (data as any).me;
        this.tipo_as = ignoreType.tipo_asentamiento;
        this.tipo = ignoreType.tipo;
        this.grad = ignoreType.grado;
        this.mncp = ignoreType.municipio;
      });
    }
  }

  onSubmit() {}

  onClose() {
    // Rest form and close popup
    this.service.form.reset();
    this.dialogRef.close();
  }

  updatePass() {
    let pass = prompt("Ingrese su nueva contraseña");
    if (pass) {
      this.service.updatePassword(this.id, pass).subscribe(
        ({ data }) => {
          if (data) {
            this.notificationService.success("Contraseña actualizada");
          } else {
            this.notificationService.warn("Hubo un problema");
          }
        },
        err => {
          this.notificationService.warn("Hubo un problema");
        }
      );
    }
  }

  tipo_asentamiento(t: string) {
    switch (t) {
      case "1":
        return "Colonia";
      case "2":
        return "Fraccionamiento";
      case "3":
        return "Barrio";
      case "4":
        return "Rancheria";
      default:
        break;
    }
  }

  municipio(mncp: string) {
    switch (mncp) {
      case "1":
        return "Santiago Suchilquitongo";
      case "2":
        return "San Francisco Telixtlahuaca";
      case "3":
        return "San Pablo Huitzo";
      case "4":
        return "Magdalena Apasco";
      default:
        break;
    }
  }

  grado(grd: string) {
    switch (grd) {
      case "1":
        return "1° Dan";
      case "2":
        return "2° Dan";
      case "3":
        return "3° Dan";
      case "4":
        return "4° Dan";
      case "5":
        return "5° Dan";
      case "6":
        return "6° Dan";
      case "7":
        return "7° Dan";
      case "8":
        return "8° Dan";
      case "9":
        return "9° Dan";
      default:
        break;
    }
  }
}
