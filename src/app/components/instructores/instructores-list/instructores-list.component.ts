import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialogRef
} from "@angular/material";
import { InstructoresComponent } from "../instructores/instructores.component";
import { InstructoresService } from "src/app/services/instructores.service";
import { InstructoresViewComponent } from "../instructores-view/instructores-view.component";
import { NotificationService } from "src/app/services/notificationService.service";

export interface InstructorData {
  _id: string;
  nombre: string;
  primerApellido: string;
  segundoApellido: string;
  fecha_nacimiento?: string;
  sexo?: string;
  n_telefono?: string;
  correo: string;
  grado?: string;
  nombre_calle?: string;
  nCasa?: string;
  codigoPostal?: string;
  estado?: string;
  ciudad?: string;
  municipio?: string;
  asentamiento?: string;
  tipo_asentamiento?: string;
}

@Component({
  selector: "app-instructores-list",
  templateUrl: "./instructores-list.component.html",
  styleUrls: ["./instructores-list.component.css"]
})
export class InstructoresListComponent implements OnInit {
  displayedColumns: string[] = [
    "ID",
    "Nombre",
    "Apellidos",
    "Correo",
    "Acciones"
  ];
  dataSource: MatTableDataSource<InstructorData>;
  instructores;
  disabled: boolean = false;
  keyString: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private _instructoresService: InstructoresService,
    private notificationService: NotificationService
  ) {
    //
    (async () => {
      await this.loadTable();
    })();
  }

  async loadTable() {
    this._instructoresService.getInstructors().subscribe(snap => {
      let array = snap
        .filter((data: any) => !data.payload.doc.data().deleted)
        .filter((data: any) => {
          return data.payload.doc.data().tipo !== "admin";
        })
        .map((data: any) => {
          // show this data
          return {
            fakeId: data.payload.doc.data().fakeId,
            _id: data.payload.doc.data().uid,
            nombre: data.payload.doc.data().nombre,
            primerApellido: data.payload.doc.data().primer_apellido,
            segundoApellido: data.payload.doc.data().segundo_apellido,
            correo: data.payload.doc.data().correo
          };
        });

      this.dataSource = new MatTableDataSource(array);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  ngOnInit() {}

  onCreate() {
    // open mat dialog, InstructoresComponent
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.height = "70%";
    this.dialog.open(InstructoresComponent, dialogConfig);
  }

  onLaunch(id: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.height = "70%";

    let dialogRef: MatDialogRef<InstructoresViewComponent> = this.dialog.open(
      InstructoresViewComponent,
      dialogConfig
    );
    // set id property in InstructoresViewComponent
    dialogRef.componentInstance.id = id;
  }

  onEdit(id: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.height = "70%";
    let dialogRef = this.dialog.open(InstructoresComponent, dialogConfig);
    // set isEdited property to true and set the id to edit
    dialogRef.componentInstance.isEdited = true;
    dialogRef.componentInstance.id = id;
  }

  onDelete(id: string): void {
    let res = confirm("¿Esta seguro de eliminar este instructor?");
    if (res) {
      this._instructoresService.deleteInstructor(id).subscribe(
        ({ data }) => {
          this.notificationService.success("Eliminado correctamente");
        },
        err => {
          this.notificationService.warn("Algo salio mal");
        }
      );
    }
  }

  onSearchClear() {
    this.keyString = "";
    this.applyFilter();
  }

  applyFilter() {
    // filter ignore uppercase
    this.dataSource.filter = this.keyString.trim().toLocaleLowerCase();
  }
}
